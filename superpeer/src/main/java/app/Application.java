package app;

import rightmesh.RightMesh;
import superpeer.Superpeer;

class Application {
  public static void main(String[] args) {
    System.out.println("hello world, I am an app");
    RightMesh rm = new RightMesh();
    Superpeer sp = new Superpeer(rm);
  }
}