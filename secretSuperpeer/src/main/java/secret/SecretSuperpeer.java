package secret;

import rightmesh.RightMesh;
import superpeer.Superpeer;
import viz.Visualizer;

public class SecretSuperpeer {
  public static void main(String[] args) {
    System.out.println("SecretSuperpeer is loading");
    RightMesh rm = new RightMesh();
    Superpeer s = new Superpeer(rm);
    Visualizer v = new Visualizer(rm);
  }
}