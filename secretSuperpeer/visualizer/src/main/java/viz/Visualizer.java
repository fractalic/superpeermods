package viz;

import rightmesh.RightMesh;

public class Visualizer {
  public Visualizer(RightMesh rm) {
    rm.action("viz");
  }
}